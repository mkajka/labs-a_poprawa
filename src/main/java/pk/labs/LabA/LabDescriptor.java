package pk.labs.LabA;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabA.Impl.Wyswietlacz";
    public static String controlPanelImplClassName = "pk.labs.LabA.Impl.Sterowanie";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.KawaInterfejs";
    public static String mainComponentImplClassName = "pk.labs.LabA.Impl.Kawa";
    // endregion

    // region P2
    public static String mainComponentBeanName = "main";
    public static String mainFrameBeanName = "app";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "silly-frame";
    // endregion
}